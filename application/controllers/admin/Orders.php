<?php
class orders extends CI_Controller {

		public function index()
		{

				//$this->load->view('admin/Login');
			if($this->session->has_userdata('id')) {
				 $this->load->model('admin/order_model');
				 $userdata['orders']=$this->order_model->display_orders();
				// $userdata['products']=$this->order_model->product_join();
				//echo '<pre>'; print_r($userdata['products']);exit;
                 $this->load->view('admin/product/approve_order',$userdata);
             }
             else
             {
             	redirect('admin/login');
             }
		}

			public function vieworder($id)
		{
			if($this->session->has_userdata('id')) {
					$this->load->model('admin/order_model');
				 $userdata['products']=$this->order_model->order_details($id);
				 $this->load->view('admin/product/vieworder',$userdata);
			 }
             else
             {
             	redirect('admin/login');
             }
	}

	public function approveorderprocess($id)
	{
		if($this->session->has_userdata('id')) {
     $this->load->model('admin/order_model');
	$this->order_model->approveorder($id);
	 $userdata['orders']=$this->order_model->display_orders();
 $this->load->view('admin/product/approve_order',$userdata);
		}
		else
             {
             	redirect('admin/login');
             }
	}

	public function rejectorderprocess($id)
	{
		if($this->session->has_userdata('id')) {
     $this->load->model('admin/order_model');
	$this->order_model->rejectorder($id);
 $userdata['orders']=$this->order_model->display_orders();
	 $this->load->view('admin/product/approve_order',$userdata);
		}
		else
             {
             	redirect('admin/login');
             }
	}
}


<?php $this->load->view('admin/header');?>

<?php $this->load->view('admin/aside');?>

<script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Orders   
        <a href="<?php echo base_url();?>admin/orders" class="btn btn-primary">Back</a>
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/prodcolor">Approve order</a></li>
     
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr><th>Product Name</th>
                  <th>Product color</th>
                  <th>Product Size</th>
                  <th>Product Price</th>
                   <th>Product Quantity</th>
                    <th>Cust Name</th>
                     <th>Cust Phone</th>
                      <th>Cust Country</th>
                       <th>Cust Adress</th>
    
               
                </tr>
                </thead>


                <tbody>
               

                	<?php foreach($products as $row) {; ?>

                <tr><td><?php echo $row['prodname'];?></td>
                 <td><?php echo $row['prodcolor'];?></td>
                  <td><?php echo $row['prodsize'];?>
                  </td>
                  <td><?php echo "PKR ". $row['prodprice'];?></td>
                  <td><?php echo $row['prodqty'];?></td>
                    <td><?php echo $row['custname'];?></td>
                      <td><?php echo $row['custphone'];?></td>
                        <td><?php echo $row['country'];?></td>
                          <td><?php echo $row['shippingadress'];?></td>
                 <!-- <td> 
                  	
                  	 <a href=""  class="edit btn btn-success" data-toggle="modal"><i class="fa fa-check-circle" data-toggle="tooltip" title="Approve"></i></a>
                   <a href="<?php echo base_url();?>orders/vieworder<?php echo $row['orderid'];?>"  class="view btn btn-primary" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" title="View"></i></a>

                  </td>  -->
                </tr>
               
           <?php }; ?>
              
                </tbody>
              </table>
            </div>
    </div> 
    </section>
  </div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

            <?php $this->load->view('admin/aside');?>
            <?php $this->load->view('admin/footer');?>
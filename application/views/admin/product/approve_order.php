

<?php $this->load->view('admin/header');?>

<?php $this->load->view('admin/aside');?>

<script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Orders   &nbsp&nbsp
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/prodcolor">Approve order</a></li>
     
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            

<div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr><th>order id</th>
                  <th>order status</th>
                  <th>Order Approval Status</th>
                  <th>payment status</th>
                  <th>Date</th>
                   <th>order total</th>
                  <th width="150">Actions</th>
               
                </tr>
                </thead>


                <tbody>
               

                	<?php foreach($orders as $row) {; ?>

                <tr><td><?php echo $row['order_id'];?></td>
                 <td><?php echo $row['order_status'];?></td>
                 <td><?php echo $row['approve_status'];?></td>
                  <td><?php echo $row['payment_status'];?>
                  </td>
                  <td><?php echo $row['order_date'];?></td>
                  <td><?php echo "PKR ". $row['order_total'];?></td>
                  <td> 
                  	<!--  -->
<?php if( $row['approve_status'] == 'approved' || $row['approve_status'] == 'rejected') { ?>
 <a href="<?php echo base_url();?>admin/orders/approveorderprocess/<?php echo $row['order_id'];?>" class="disabled btn btn-success" data-toggle="modal"><i class="fa fa-check-circle" data-toggle="tooltip" title="Approve"></i></a>

            <a href="<?php echo base_url();?>admin/orders/rejectorderprocess/<?php echo $row['order_id'];?>"class="disabled btn btn-danger"  class="edit btn btn-danger"data-toggle="modal"><i class="fa fa-times" data-toggle="tooltip" title="Reject"></i></a>

            <a href="<?php echo base_url();?>admin/orders/vieworder/<?php echo $row['order_id'];?>"  class="view btn btn-primary" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" title="View"></i></a>

          <?php }else{ ?>
          <a href="<?php echo base_url();?>admin/orders/approveorderprocess/<?php echo $row['order_id'];?>"  class="btn btn-success" data-toggle="modal"><i class="fa fa-check-circle" data-toggle="tooltip" title="Approve"></i></a>
                      <a href="<?php echo base_url();?>admin/orders/rejectorderprocess/<?php echo $row['order_id'];?>"  class="edit btn btn-danger" data-toggle="modal"><i class="fa fa-times" data-toggle="tooltip" title="Reject"></i></a>
                   <a href="<?php echo base_url();?>admin/orders/vieworder/<?php echo $row['order_id'];?>"  class="view btn btn-primary" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" title="View"></i></a>

                  </td><?php }?>
                </tr>
               
           <?php }; ?>
              
                </tbody>
              </table>
            </div>
    </div> 
    </section>
  </div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

            <?php $this->load->view('admin/aside');?>
            <?php $this->load->view('admin/footer');?>
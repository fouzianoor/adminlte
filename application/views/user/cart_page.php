 <?php $this->load->view('user/common/header');?>
        <div class="body__overlay"></div>
        <!-- Start Offset Wrapper -->
        <script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>
        <div class="offset__wrapper">
            <!-- Start Search Popap -->
            <div class="search__area">
                <div class="container" >
                    <div class="row" >
                        <div class="col-md-12" >
                            <div class="search__inner">
                                <form action="#" method="get">
                                    <input placeholder="Search here... " type="text">
                                    <button type="submit"></button>
                                </form>
                                <div class="search__close__btn">
                                    <span class="search__close__btn_icon"><i class="zmdi zmdi-close"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Search Popap -->
            <!-- Start Cart Panel -->
            <div class="shopping__cart">
          
                <div class="shopping__cart__inner">
                    <div class="offsetmenu__close__btn">
                        <a href="#"><i class="zmdi zmdi-close"></i></a>
                    </div>
                    <div class="shp__cart__wrap">
                        <div class="shp__single__product">
                            <div class="shp__pro__thumb">
                                <a href="#">
                                    <img src="<?php echo base_url();?>assets2/images/product-2/sm-smg/1.jpg" alt="product images">
                                </a>
                            </div>
                            <div class="shp__pro__details">
                                <h2><a href="product-details.html">BO&Play Wireless Speaker</a></h2>
                                <span class="quantity">QTY: 1</span>
                                <span class="shp__price">$105.00</span>
                            </div>
                            <div class="remove__btn">
                                <a href="#" title="Remove this item"><i class="zmdi zmdi-close"></i></a>
                            </div>
                        </div>
                        <div class="shp__single__product">
                            <div class="shp__pro__thumb">
                                <a href="#">
                                    <img src="images/product-2/sm-smg/2.jpg" alt="product images">
                                </a>
                            </div>
                            <div class="shp__pro__details">
                                <h2><a href="product-details.html">Brone Candle</a></h2>
                                <span class="quantity">QTY: 1</span>
                                <span class="shp__price">$25.00</span>
                            </div>
                            <div class="remove__btn">
                                <a href="#" title="Remove this item"><i class="zmdi zmdi-close"></i></a>
                            </div>
                        </div>
                    </div>
                    <ul class="shoping__total">
                        <li class="subtotal">Subtotal:</li>
                        <li class="total__price">$130.00</li>
                    </ul>
                    <ul class="shopping__btn">
                        <li><a href="<?php echo base_url();?>/user/cart">View Cart</a></li>
                       <li class="shp__checkout">
                        <script src="<?php echo base_url(); ?>assets/plugins/swal/swal.all.min.js"></script>_checkout"><a href="<?php echo base_url();?>checkout/order_info">Checkout</a></li>
                    </ul>
                </div>
            </div>
            <!-- End Cart Panel -->
        </div>
        <!-- End Offset Wrapper -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(<?php echo base_url();?>assets2/images/breadcrum.jpg) no-repeat scroll center center / cover ;">
            <div class="ht__bradcaump__wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="bradcaump__inner">
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="<?php echo base_url();?>products">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-chevron-right"></i></span>
                                  <span class="breadcrumb-item active">shopping cart</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- cart-main-area start -->
        <div class="cart-main-area ptb--100 bg__white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                                   
                            <div class="table-content table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail">products</th>
                                            <th class="product-name">name of products</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                            <th class="product-remove">Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <form action="<?php echo base_url();?>cart/update_cart" method="post">
                                         <?php
                                         $total_price =0.0;
                                 $cart = $this->cart->contents();
                                 foreach($cart as $indice => $cartprod){
                                    $total_price += $cartprod['subtotal'];
                                 ?>
                                        <tr>
                                            <td class="product-thumbnail"><a href="#"><img src="<?php echo base_url();?>uploads/products/<?= $cartprod['image'];?>" alt="product img" /></a></td>
                                            <td class="product-name"><a href="#"><?php echo $cartprod['name'];?></a>
                                                <ul  class="pro__prize">
                                                    <li class="old__prize"><?= $cartprod['sale_price'];?></li>
                                                    <li><?= $cartprod['price']?></li>
                                                </ul>
                                            </td>
                                            
                                            <td class="product-price"><span class="amount"><?=$cartprod['price']?></span></td>
                                           

                                           <td class="product-quantity"><input type="number" value="<?= $cartprod['qty'];?>" name='qty[]' /></td>
                                           <input type="hidden" name="rowid[]" value="<?= $cartprod['rowid'];?>"/>
 
                                            <td class="product-subtotal"><?=$cartprod['subtotal']?></td>
                                          

                                           <td>   <a  href="<?php echo base_url();?>cart/delete_cartitem/<?=$cartprod['rowid'];?>" class="delete btn btn-danger" data-toggle="modal"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a></td>



                                        </tr>
                                    <?php };?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="buttons-cart--inner">
                                        <div class="buttons-cart">
                                            <a href="<?php echo base_url();?>products">Continue Shopping</a>
                                        </div>
                                    
                                          <input type="submit" value="Update " class="btn btn-default" style="width: 20%;" >
                                        
                                         </form> 
                                   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="ht__coupon__code">
                                        <span>enter your discount code</span>
                                        <div class="coupon__box">
                                            <input type="text" placeholder="">
                                            <div class="ht__cp__btn">
                                                <a href="#">enter</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 smt-40 xmt-40">
                                    <div class="htc__cart__total">
                                        <h6>cart total</h6>
                                        <div class="cart__desk__list">
                                            <ul class="cart__desc">
                                                <li>cart total</li>
                                                <li>tax</li>
                                                <li>shipping</li>
                                            </ul>
                                            <ul class="cart__price">
                                                <li><?=$total_price;?></li>
                                                <li>16%</li>
                                                <li>1000</li>
                                            </ul>
                                        </div>
                                        <div class="cart__total">
                                            <?php $total1 = $total_price*(16/100) ;
                                            $total = $total1 + $total_price + 1000 ;?>
                                            <span>order total</span>
                                            <span><?=$total?></span>

                                        </div>
                                        <ul class="payment__btn">
                                            <li class="active"><a href="<?php echo base_url();?>cart/order_info/<?=$total;?>">checkout</a></li>


                                            <li><a href="<?php echo base_url();?>products">continue shopping</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                  
                    </div>
                </div>
            </div>
        </div>
        <!-- cart-main-area end -->
        <!-- Start Brand Area -->
       <?php $this->load->view('user/common/section');?>
        <!-- End Brand Area -->
        <!-- Start Banner Area -->
        <div class="htc__banner__area">
            <ul class="banner__list owl-carousel owl-theme clearfix">
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/1.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/2.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/3.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/4.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/5.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/6.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/1.jpg" alt="banner images"></a></li>
                <li><a href="product-details.html"><img src="<?php echo base_url();?>assets2/images/banner/bn-3/2.jpg" alt="banner images"></a></li>
            </ul>
        </div>
        <!-- End Banner Area -->
        <!-- End Banner Area -->
        <!-- Start Footer Area -->

<script>
$(document).on("click", ".delete", function(e) {
        var tag = this;
        e.preventDefault();
        swal.fire({
          title: 'Are you sure you want to delete this product from cart?',
          text: "This action cannot be undone and all the related data will be deleted",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
          if (result.value) {
            window.location.href = $(tag).attr("href");
          } else {
            return false;
          }
        });
      });
  </script>
      <?php $this->load->view('user/common/footer');?>